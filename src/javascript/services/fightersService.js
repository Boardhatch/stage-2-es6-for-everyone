import { callApi } from '../helpers/apiHelper';
import { getFighterById } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      return getFighterById('details/fighter/'+ id +".json");
    } catch (error) {
      console.warn(error);
    } 
    
  }
}

export const fighterService = new FighterService();
