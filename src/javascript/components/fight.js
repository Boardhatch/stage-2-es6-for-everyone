import { controls } from '../../constants/controls';
import { createElement } from "../../javascript/helpers/domHelper";

export async function fight(firstFighter, secondFighter) { 
  return new Promise((resolve) => {
    let isFirstBlock = false;
    let isSecondBlock = false;
    const firstFighterHealth = firstFighter.health;
    const secondFighterHealth = secondFighter.health;
    let firstBackgroundHPbar =  document.querySelector("#left-fighter-indicator");
    let secondBackgroundHPbar =  document.querySelector("#right-fighter-indicator");
    firstBackgroundHPbar.style.backgroundColor = "red";
    secondBackgroundHPbar.style.backgroundColor = "red";
    let firstHPbar = createElement({tagName: 'div', className: 'arena___gren-health-bar'});
    let secondHPbar = createElement({tagName: 'div', className: 'arena___gren-health-bar'});
    firstBackgroundHPbar.append(firstHPbar);
    secondBackgroundHPbar.append(secondHPbar);

    const check = () => {
      if (firstFighter.health <= 0 || secondFighter.health <=0) {
        resolve(firstFighter.health <= 0 ? secondFighter : firstFighter);
      }
    };

    document.addEventListener('keyup', function(event) {
      if (event.code == controls.PlayerOneAttack) {
        if (!isFirstBlock && !isSecondBlock) {
          let damage = getDamage(firstFighter, secondFighter);
          if (damage > 0) {
            secondFighter.health -= damage;
          }
          let widthPersent = (secondFighter.health * 100) / secondFighterHealth;
          secondHPbar.style.width = (widthPersent > 0 ? widthPersent : 0) + "%";
        }
        check();
      }
      if (event.code == controls.PlayerTwoAttack) {
        if (!isFirstBlock && !isSecondBlock) {
          let damage = getDamage(secondFighter, firstFighter);
          if (damage > 0) {
            firstFighter.health -= damage;
          }
          let widthPersent = (firstFighter.health * 100) / firstFighterHealth;
          firstHPbar.style.width = (widthPersent > 0 ? widthPersent : 0) + "%";
        }
        check();
      }
      if (event.code == controls.PlayerOneBlock) {
        isFirstBlock = false;
      }
      if (event.code == controls.PlayerTwoBlock) {
        isSecondBlock = false;
      }
    });
    document.addEventListener('keydown', function(event) {
      if (event.code == controls.PlayerOneBlock) {
        isFirstBlock = true;
      }
      if (event.code == controls.PlayerTwoBlock) {
        isSecondBlock = true;
      }
    });
    let firstFighterComboTime;
    let secondFighterComboTime;
    document.addEventListener('keydown', function(event) {

      if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
        if (firstFighterComboTime == undefined || (new Date().getSeconds() - firstFighterComboTime) > 10) {
          let damage = 2 * firstFighter.attack;
          secondFighter.health -= damage;
          let widthPersent = (secondFighter.health * 100) / secondFighterHealth;
          secondHPbar.style.width = (widthPersent > 0 ? widthPersent : 0) + "%";
          firstFighterComboTime = new Date().getSeconds();
        }
        check();
      }
      if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
        if (secondFighterComboTime == undefined || (new Date().getSeconds() - secondFighterComboTime) > 10) {
          let damage = 2 * secondFighter.attack;
          firstFighter.health -= damage;
          let widthPersent = (firstFighter.health * 100) / firstFighterHealth;
          firstHPbar.style.width = (widthPersent > 0 ? widthPersent : 0) + "%";
          secondFighterComboTime = new Date().getSeconds();
        }
        check();
      }
    });
    
    
  });
}

export function getDamage(attacker, defender) {
  let res = getHitPower(attacker) - getBlockPower(defender);
  return res < 0 ? 0 : res;
}

export function getHitPower(fighter) {
  return fighter.attack * ((Math.random() * 2) + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * ((Math.random() * 2) + 1);
}
