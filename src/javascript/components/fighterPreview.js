import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const outerContainer = createElement({
    tagName: "div",
    className: "outer-container"
  });
  
  if (fighter != undefined) {
    let fighterImg = createFighterImage(fighter);
    const fighterInfo = createFighterInfoTable(fighter);
    fighterElement.append(fighterImg);
    if (position == "right") {
      fighterImg.style.transform = "scaleX(-1)";
      outerContainer.append(fighterElement);
      outerContainer.append(fighterInfo);
    } else {
      outerContainer.append(fighterInfo);
      outerContainer.append(fighterElement);
    }
  }

  return outerContainer;
}

function createFighterInfoTable(fighter) {
  const table = createElement({
    tagName: "table",
    className: "fighter-table"
  });
  table.innerHTML = "<caption>Fighter</caption>"
  for (const [key, value] of Object.entries(fighter)) {
    if (`${key}` != "_id" && `${key}` != "source") {
      table.innerHTML += appendRow(`${key}`, `${value}`)
    }
  }
  return table;
}

function appendRow(key, value) {
  return "<tr><th>" + key + "</th><th>" + value + "</th></tr>"
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___root',
    attributes,
  });

  return imgElement;
}
