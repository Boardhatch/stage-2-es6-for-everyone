import { showModal } from '../modal/modal'
import { createElement } from '../../helpers/domHelper';
export function showWinnerModal(fighter) {
  const { name, source: src } = fighter;
  const elem = createElement({tagName: "img", className: "modal-body", attributes: {src}});
  const title = "Переможець - " + name + ". \nПоклав тебе на лопатки";
  showModal({title: title, bodyElement: elem});
}
